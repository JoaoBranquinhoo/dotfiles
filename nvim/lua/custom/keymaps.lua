local M = {}
-- [[ Basic Keymaps ]]
-- See `:help vim.keymap.set()`
vim.keymap.set({ 'n', 'v' }, '<Space>', '<Nop>', { silent = true })
-- Remap for dealing with word wrap
-- Normal Mode
vim.keymap.set('n', 'k', "v:count == 0 ? 'gk' : 'k'", { expr = true, silent = true })
vim.keymap.set('n', 'j', "v:count == 0 ? 'gj' : 'j'", { expr = true, silent = true })
vim.keymap.set('n', "<C-h>", "<C-w>h", {  silent = true })
vim.keymap.set('n', '<C-l>', "<C-w>l", {  silent = true })
vim.keymap.set('n', '<C-j>', "<C-w>j", {  silent = true })
vim.keymap.set('n', '<C-k>', "<C-w>k", {  silent = true })
--resize window
vim.keymap.set('n', '<C-Up>', "<Cmd>resize -2<cr>", {  silent = true })
vim.keymap.set('n', '<C-Down>', "<Cmd>resize +2<cr>", {  silent = true })
vim.keymap.set('n', '<C-Right>', "<Cmd>vertical resize +2<cr>", {  silent = true })
vim.keymap.set('n', '<C-Left>', "<Cmd>vertical resize -2<cr>", {  silent = true })
-- Move current line / block with Alt-j/k a la vscode.
vim.keymap.set('n', '<A-j>', "<Cmd>m .+1<CR>==", {  silent = true })
vim.keymap.set('n', '<A-k>', "<Cmd>m .-2<CR>==", {  silent = true })
--navigate buffer
vim.keymap.set('n', "<S-h>", "<Cmd>tabprevious<cr>", {  silent = true })
vim.keymap.set('n', "<S-l>", "<Cmd>tabnext<cr>", {  silent = true })

-- VISUAL Mode
vim.keymap.set('v', "x", "d", {  silent = true })
--
-- Visual Block Mode
vim.keymap.set('x', '<A-j>', ":m '>+1<CR>gv-gv", {  silent = true })
vim.keymap.set('x', '<A-k>', ":m '<-2<CR>gv-gv", {  silent = true })

-- Diagnostic keymaps
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, { desc = 'Go to previous diagnostic message' })
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, { desc = 'Go to next diagnostic message' })
vim.keymap.set('n', '<leader>de', vim.diagnostic.open_float, { desc = 'Open floating diagnostic message' })
vim.keymap.set('n', '<leader>dq', vim.diagnostic.setloclist, { desc = 'Open diagnostics list' })
return M
