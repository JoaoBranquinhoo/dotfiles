local M = {}
local wk = require("which-key")

wk.register({
  d = {
    name = "Diagnostics", -- optional group name
  },
  s = {
    name = "Search", -- optional group name
  },
  o = {
    name = "Orgmode", -- optional group name
  },
  t = {
    name = "Table mode", -- optional group name
  },
  -- g = {
  --   name = "Git", -- optional group name
  -- },
  h = {
    name = "Highlight toggle", -- optional group name
  },

  l = {
    name = "LSP", -- optional group name
  },
}, { prefix = "<leader>" })
return M
