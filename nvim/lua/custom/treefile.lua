local M = {}
-- disable netrw at the very start of your init.lua
vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1
local api = require("nvim-tree.api")
local treeutils = require("custom.treeutils")

local function edit_or_open()
  local node = api.tree.get_node_under_cursor()

  if node.nodes ~= nil then
    -- expand or collapse folder
    api.node.open.edit()
  else
    -- open file
    api.node.open.tab(node)
    -- Close the tree if file was opened
    api.tree.close()
  end
end

-- open as vsplit on current node
local function vsplit_preview()
  local node = api.tree.get_node_under_cursor()

  if node.nodes ~= nil then
    -- expand or collapse folder
    api.node.open.edit()
  else
    -- open file as vsplit
    api.node.open.vertical()
  end

  -- Finally refocus on tree if it was lost
  api.tree.focus()
end

local function change_to_buffer_dir()
  local buffer_path =vim.api.nvim_buf_get_name(bufnr)
  local i = string.find(buffer_path,string.match(buffer_path, "/%w+%.%w+"))
  local buffer_dir = string.sub(buffer_path, 1, i)
  return api.tree.change_root(buffer_dir)
end

local function my_on_attach(bufnr)
  local api = require "nvim-tree.api"
  local function opts(desc)
    return { desc = "nvim-tree: " .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  -- default mappings
  api.config.mappings.default_on_attach(bufnr)
  print(bufnr)


  -- custom mappings

  -- vim.keymap.set('n', 'c', change_to_buffer_dir, opts('Change root to buffer dir'))
  vim.keymap.set('n', '?', api.tree.toggle_help, opts('Help'))
  vim.keymap.set("n", "l", edit_or_open, opts("Edit Or Open"))
  vim.keymap.set('n', 'P',     api.node.navigate.parent,              opts('Parent Directory'))
  vim.keymap.set("n", "L", vsplit_preview, opts("Vsplit Preview"))
  vim.keymap.set("n", "H", api.tree.collapse_all, opts("Collapse All"))
  vim.keymap.set('n', '<c-f>', treeutils.launch_find_files, opts('Launch Find Files'))
  vim.keymap.set('n', '<c-g>', treeutils.launch_live_grep, opts('Launch Live Grep'))


end

vim.keymap.set('n', '<leader>e', "<Cmd>NvimTreeToggle<cr>", { desc = 'Open File Tree' })
require("nvim-tree").setup({
  sort_by = "case_sensitive",
  view = {
    width = 44,
  },
  renderer = {
    group_empty = true,
  },
  filters = {
    dotfiles = true,
  },
  on_attach = my_on_attach,
  update_focused_file = {
    enable = true,
    update_root = true,
    ignore_list = {},
  },
})


return M
