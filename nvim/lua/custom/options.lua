-- [[ Setting options ]]
-- See `:help vim.o`
local M = {}

-- Set highlight on search
vim.o.hlsearch = true
-- Make line numbers default
vim.wo.relativenumber = true
vim.wo.number = true
-- Enable mouse mode
vim.o.mouse = 'a'
-- Sync clipboard between OS and Neovim.
vim.o.clipboard = 'unnamedplus'
-- Enable break indent
vim.o.breakindent = true
-- Save undo history
vim.o.undofile = true
-- Case-insensitive searching UNLESS \C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true
-- Keep signcolumn on by default
vim.wo.signcolumn = 'yes'
-- Decrease update time
vim.o.updatetime = 250
vim.o.timeoutlen = 300
-- Set completeopt to have a better completion experience
vim.o.completeopt = 'menuone,noselect'
-- NOTE: You should make sure your terminal supports this
vim.o.termguicolors = true
vim.o.hidden = true -- required to keep multiple buffers and open multiple buffers
vim.o.pumheight = 10 -- pop up menu height
vim.o.cursorline = true -- highlight the current line
vim.o.wrap = true -- display lines as one long line
vim.o.linebreak = true
vim.o.scrolloff = 8 -- minimal number of screen lines to keep above and below the cursor.
vim.o.sidescrolloff = 8 -- minimal number of screen lines to keep left and right of the cursor.


-- [[ Highlight on yank ]]
-- See `:help vim.highlight.on_yank()`
local highlight_group = vim.api.nvim_create_augroup('YankHighlight', { clear = true })
vim.api.nvim_create_autocmd('TextYankPost', {
  callback = function()
    vim.highlight.on_yank()
  end,
  group = highlight_group,
  pattern = '*',
})
return M;
