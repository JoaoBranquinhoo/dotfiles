vim.g.mapleader = ' '
vim.g.maplocalleader = ' '

require('custom.plugins')
require('custom.options')
require('custom.keymaps')
require('custom.telescope')
-- require('custom.treesitter')
require('custom.treefile')
require('custom.lsp')
require('custom.which-key')
vim.opt.conceallevel = 2
vim.opt.concealcursor = 'nc'


require("luasnip.loaders.from_vscode").lazy_load()
require("toggleterm").setup{
  open_mapping = [[<c-\>]],
  direction = "float",
  float_opts = {
    -- The border key is *almost* the same as 'nvim_open_win'
    -- see :h nvim_open_win for details on borders however
    -- the 'curved' border is a custom border type
    -- not natively supported but implemented in this plugin.
    border = 'single',
    width = 90,
    height = 30,
    winblend = 10,
    zindex = 10,
  },
}
